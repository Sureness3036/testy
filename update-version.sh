#!/bin/sh

shellcheck ./update-version.sh

get_largest_version() { grep "^v[0-9]\+$" | tr -d v | sort -n | tail -n1; }

current_version=$(git tag -l --contains HEAD | get_largest_version)

if [ -z "$current_version" ]; then
    highest_version=$(git tag | get_largest_version)

    this_version=$(( highest_version + 1))

    git tag -a v$this_version -m "Incrementing tag"
fi