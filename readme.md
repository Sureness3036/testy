# Ops Git Workflows




## Problem



## Current BitBucket

### Operations Workflow

Manual:
1. Pull latest source from CVS.
2. Work on code, and schedule in-person/zoom reviews as necessary until stakeholders are satisfied with changes.
3. Commit and push code back to CVS.
4. Deploy code by checking out procs and a cascading set of rsyncs.
5. Run verion check in terminal to verfy it is the version you are expecting.
6. Run code. OASIS will log git version metadata like version, and commit.


## Current CVS

### Operations Workflow

Manual:
1. Pull latest source from CVS.
2. Work on code, and schedule in-person/zoom reviews as necessary until stakeholders are satisfied with changes.
3. Commit and push code back to CVS.
4. Deploy code by checking out procs and a cascading set of rsyncs.
5. Run verion check in terminal to verfy it is the version you are expecting.
6. Run code. OASIS will log git version metadata like version, and commit.


## Spirit Proposal


### Operations Workflow

Manual:
1. Create a git feature branch and open a work in progress pull request on BitBucket.
2. Work on code, adding reviewers as necessary, until both you and your reviewers are satisfied with changes.
3. Merge pull request to main branch.

Automatic Steps:

4. TeamCity git tags the commit now in main with an incremented version number. Additionally, it signs the tag with a PGP key.
5. TeamCity pushes changes to bare git repo on dev net.
6. Dev net cascade syncs bare repos on each net.

Manual:

7. Git checkout version. If git does not throw an error, you have the same code.
8. Run code. OASIS will log git version metadata like version, and commit.


### Build Code

- [update-version.sh](./update-version.sh)
- [deploy.sh](./update-version.sh)


### Tamper Proof

PGP signed tags and SHA-256 git hashes ensure that the code in each repo cannot be tampered end to end between the Bitbucket repo and deployment.


### Comparison

feature | Spirit | CVS/BB | CVS
 --- | --- | --- | ---
Tamper Proof | &#9745; | &#x2612; | &#x2612;
Automatic Deployment | &#9745; | &#x2612; | &#x2612;
Version Validation | &#9745; | &#x2612; | &#x2612;
Enforced Code Review | &#9745; | &#x2612; | &#x2612;
Granular Merge Permissions | &#9745; | &#x2612; | &#x2612;
Operator Burden | Low | High | Medium
Software Maintenance | Low | Low | Low
Individually Versioned Files | &#x2612; | &#9745; | &#9745;


# Reading

- https://mermaid.js.org/syntax/flowchart.html
- https://pandoc.org/chunkedhtml-demo/10-slide-shows.html
- https://en.wikipedia.org/wiki/Spacecraft_command_language
- https://ntrs.nasa.gov/api/citations/19940019140/downloads/19940019140.pdf
- https://www.gnu.org/software/trans-coord/manual/cvs/html_node/Keyword-list.html#Keyword-list